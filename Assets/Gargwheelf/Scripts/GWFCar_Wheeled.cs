﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargWheelf
{
	public class GWFCar_Wheeled : GWFCar
	{
		protected override void FixedUpdate()
		{
			if (activeVehicle && whlWgt > 0)
			{
				//Apply forward momentum
				r.velocity += t.forward * verticalAxis * whlWgt0 * accelleration * curve.Evaluate(velocity * msDiv) * Time.fixedDeltaTime;
				
				//Apply steering torque
				float yv = horizontalAxis * whlWgt1 * velocity * hspdwMult * steeringAngle;
				if (yv < 0 ? t.InverseTransformDirection(r.angularVelocity).y > yv : t.InverseTransformDirection(r.angularVelocity).y < yv)
					r.angularVelocity += t.up * yv * Time.fixedDeltaTime * 10;
			}
			
			base.FixedUpdate();
		}
	}
}
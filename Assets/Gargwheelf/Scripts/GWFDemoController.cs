﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargWheelf
{
	public class GWFDemoController : MonoBehaviour
	{
		public GWFCar car;
		void Update()
		{
			car.verticalAxis = Input.GetAxis("Vertical"); //W and S
			car.horizontalAxis = Input.GetAxis("Horizontal"); //A and D
			car.braking = Input.GetKey(KeyCode.Space); //Brake on holding the spacebar
			
			//Prevent the vehicle from getting stuck flipped upside down
			float z = car.t.eulerAngles.z;
			if (z > 180)
				z -= 360;
			if ((z > 89 || z < -89) && car.r.velocity.sqrMagnitude < .001f && car.r.angularVelocity.sqrMagnitude < .001f)
				car.t.eulerAngles = new Vector3(car.t.eulerAngles.x, car.t.eulerAngles.y, 0);
		}
	}
}

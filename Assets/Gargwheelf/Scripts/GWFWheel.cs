﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargWheelf
{
	public enum GWFWheelType
	{
		TrackedRight = 0, //Moves forward and back based on steering and acceleration
		TrackedLeft = 1, //Moves forward and back based on steering and acceleration
		WheeledTorque = 2, //Moves forward and back based on acceleration
		WheeledSteering = 3, //Moves forward and back based on torque, turns left and right based on steering
		WheeledTorqueSteering = 4 //Moves forward and back based on acceleration, turns left and right based on steering
	}
	public class GWFWheel : MonoBehaviour
	{
		[Header("Make sure wheels only have triggers at most")]
		public GWFCar car;
		public bool isGrounded;
		public GWFWheelType wheelType;
		[Header("The following is automatically set by the code")]
		[Header("Use it when referencing the wheels' transforms")]
		[Header("in other scripts you yourself write")]
		public Transform t;
		Vector3 localPos;
		float radMult;
		void Start()
		{
			//transform is just a hidden GetComponent call, so we cache it into a variable
			t = transform;
			//Get our initial position, from which everything else is derived from 
			localPos = t.localPosition;
			//This is used for the purely visual effect of the wheels spinning
			radMult = 45 / car.wheelRadius;
		}
		float xrot;
		void FixedUpdate()
		{
			RaycastHit hit;
			//Reset the position to as high as it can possibly go
			t.localPosition = localPos + Vector3.up * car.suspensionGive * .25f;
			//Cast a single ray to the vehicle's down axis
			if (Physics.Raycast(t.position, -car.t.up, out hit, car.suspensionGive, ~(1 << gameObject.layer)))
			{
				//Set our position based on where we hit
				t.position = Vector3.MoveTowards(t.position, hit.point, car.suspensionGive);
				//Apply a force that's proportional to the distance but not dependend on it
				float force = (car.suspensionGive - Vector3.Distance(localPos, t.localPosition)) / car.suspensionGive;
				car.r.AddForceAtPosition(car.t.up * force * force * force * car.suspensionSpring, t.position, ForceMode.Acceleration);
				isGrounded = true;
			}
			else
			{
				//We aren't touching the ground, so our spring is fully stretched out
				t.position -= car.t.up * car.suspensionGive;
				isGrounded = false;
			}
			//Displace it so the surface is touching the ground rather than the centre clipping into it
			//Note: You can move this to either of the previous two to save a small amount of computing time
			t.position += car.t.up * car.wheelRadius;
			
			//X-axis rotation stuff. Don't worry about it :3
			if ((int)wheelType > 1 && !car.braking)
			{
				xrot += car.velocity * radMult * Time.fixedDeltaTime;
				if ((int)wheelType != 3 && !isGrounded)
					xrot += car.verticalAxis * car.maxSpeed * Time.fixedDeltaTime * 360;
			}
			else if (!car.braking || wheelType == GWFWheelType.WheeledSteering)
				xrot += (car.velocity + car.r.angularVelocity.y * ((int)wheelType == 0 ? -1 : 1) * 4) * Time.fixedDeltaTime * radMult;
			
			xrot = xrot % 360; //Don't lose precision!
			
			//Apply the X-axis rotation
			t.localEulerAngles = Vector3.right * xrot;
			//Rotate based on steering if applicable 
			if ((int)wheelType > 2)
				t.Rotate(car.t.up * car.steeringAngle * car.horizontalAxis, Space.World);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargWheelf
{
	public class GWFCar_Tracked : GWFCar
	{
		protected override void FixedUpdate()
		{
			if (activeVehicle && whlWgt > 0 && !braking)
			{
				//Lower both axes if both are non-zero
				float v = verticalAxis * Mathf.Lerp(1, .5f, horizontalAxis * horizontalAxis);
				float h = horizontalAxis * Mathf.Lerp(1, .5f, verticalAxis * verticalAxis);
				
				//Add forward momentum
				r.velocity += t.forward * v * whlWgt0 * accelleration * curve.Evaluate(velocity * msDiv) * Time.fixedDeltaTime;
				//Add steering torque
				float yv = (h + (whlWgt1 - whlWgt0)) * maxSpeed * hspdtMult;
				if (yv < 0 ? t.InverseTransformDirection(r.angularVelocity).y > yv : t.InverseTransformDirection(r.angularVelocity).y < yv)
					r.angularVelocity += t.up * yv * Time.fixedDeltaTime * 10;
			}
			
			base.FixedUpdate();
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargWheelf
{
	public class GWFCar : MonoBehaviour
	{
		[Header("Set these")]
		public Rigidbody r;
		public Transform t;
		public float maxSpeed = 104;
		public float accelleration = 10;
		public float steeringAngle = 30;
		protected AnimationCurve curve;
		[Range(0, 1)]
		public float driftCoefficient = .621f;
		public float wheelRadius = .5f;
		public float suspensionGive = 1.37f;
		public float suspensionSpring = 9;
		public float suspensionDrag = 1;
		public GWFWheel[] wheels;
		[Header("Script these")]
		public float velocity;
		public bool activeVehicle;
		public float verticalAxis;
		public float horizontalAxis;
		public bool braking;
		[Header("Ignore these")]
		public float whlDiv;
		protected float msDiv;
		protected virtual void Start()
		{
			whlDiv = 1 / (wheels.Length * 1.0f);
			msDiv = 1 / maxSpeed;
			
			Vector3 com = Vector3.up;
			foreach (var e in wheels)
				com += e.transform.localPosition * whlDiv;
			r.centerOfMass = com;
			
			curve = new AnimationCurve();
			curve.AddKey(0, 1);
			curve.AddKey(1, 0);
		}
		public static float hspdwMult = .005f;
		public static float hspdtMult = .01f;
		protected float whlWgt;
		protected float whlWgt0;
		protected float whlWgt1;
		protected virtual void FixedUpdate()
		{
			Vector3 localisedVelocity = t.InverseTransformDirection(r.velocity);
			velocity = localisedVelocity.z;
			
			whlWgt = 0;
			whlWgt0 = 0;
			whlWgt1 = 0;
			foreach (var e in wheels)
			{
				if (e.isGrounded)
				{
					whlWgt += whlDiv;
					switch (e.wheelType)
					{
						case GWFWheelType.TrackedRight:
						case GWFWheelType.WheeledTorque:
							whlWgt0 += whlDiv;
							break;
							
						case GWFWheelType.TrackedLeft:
						case GWFWheelType.WheeledSteering:
							whlWgt1 += whlDiv;
							break;
							
						case GWFWheelType.WheeledTorqueSteering:
							whlWgt0 += whlDiv;
							whlWgt1 += whlDiv;
							break;
					}
				}
			}
			if (whlWgt > 0)
			{
				r.velocity = t.TransformDirection(new Vector3(localisedVelocity.x - (1 - driftCoefficient) * whlWgt * Time.fixedDeltaTime * 32 * Mathf.Clamp(localisedVelocity.x, -1, 1), localisedVelocity.y, localisedVelocity.z));
				r.velocity = new Vector3(r.velocity.x, r.velocity.y * (1 - whlWgt * suspensionDrag * Time.fixedDeltaTime), r.velocity.z);
				r.angularVelocity *= 1 - whlWgt * suspensionDrag * 2 * Time.fixedDeltaTime;
				r.angularVelocity -= t.forward * (localisedVelocity.x + r.angularVelocity.y) * Time.fixedDeltaTime;
				
				if (braking)
				{
					if (velocity * velocity > 1)
						r.velocity -= r.velocity.normalized * whlWgt * 10 * Time.fixedDeltaTime * Mathf.Clamp01(r.velocity.magnitude);
					else //Use a cheaper version of the brake code if the car is already going slow (e.g. is parked)
						r.velocity *= 1 - whlWgt * 10 * Time.fixedDeltaTime;
				}
			}
		}
	}
}